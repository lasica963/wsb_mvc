
from factories import factories

items = list()


def get_people():
    global items
    return [item for item in items]


def add_person(name, surname, age):
    global items
    results = list(filter(lambda x: x['name'] == name, items))
    if results:
        raise factories.ItemAlreadyExists('"{}" już istnieje!'.format(name))
    else:
        items.append({'name': name, 'surname': surname, 'age': age})


def create_items(app_items):
    global items
    items = app_items


def read_item(name):
    global items
    my_items = list(filter(lambda x: x['name'] == name, items))
    if my_items:
        return my_items[0]
    else:
        raise factories.ItemNotExists(
            'Can\'t read "{}" because it\'s not stored'.format(name))


def update_user(name, surname, age):
    global items
    list_of__items = list(
        filter(lambda i_x: i_x[1]['name'] == name, enumerate(items)))
    if list_of__items:
        i, item_to_update = list_of__items[0][0], list_of__items[0][1]
        items[i] = {'name': name, 'surname': surname, 'age': age}
    else:
        raise factories.ItemNotExists(
            'Nie można zaaktualizować, ponieważ nie ma takiego użytkownika w bazie'.format(name))


def delete_user(name):
    global items
    list_of__items = list(
        filter(lambda i_x: i_x[1]['name'] == name, enumerate(items)))
    if list_of__items:
        i, item_to_delete = list_of__items[0][0], list_of__items[0][1]
        del items[i]
    else:
        raise factories.ItemNotExists(
            'Nie można usunąć, ponieważ nie ma takiego użytkownika w bazie'.format(name))
