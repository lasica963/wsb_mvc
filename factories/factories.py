class ItemAlreadyExists(Exception):
    pass


class ItemNotExists(Exception):
    pass
