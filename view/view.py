# model_view_controller.py
class View(object):

    @staticmethod
    def show_person_list(items):
        for i, item in enumerate(items):
            print(i+1, item['name'], item['surname'], item['age'])

    @staticmethod
    def show_person(item, item_info):
        print('Oto użytkownik {}'.format(item.upper()))
        print('{} Dane osobowe: {}'.format(1, item_info))

    @staticmethod
    def user_missing_error(item, err):
        print('Nie ma {} w bazie!'.format(item.upper()))
        print('{}'.format(err.args[0]))

    @staticmethod
    def user_already_stored_error(item, err):
        print('Mamy {} w bazie'
              .format(item.upper()))
        print('{}'.format(err.args[0]))

    @staticmethod
    def user_not_yet_stored_error(item, err):
        print('Nie mamy takie użytkownika w bazie!'
              .format(item.upper()))
        print('{}'.format(err.args[0]))

    @staticmethod
    def user_storied(item):
        print('Użytkownik {} został dodany'
              .format(item.upper()))

    @staticmethod
    def user_updated(item, old_surname, old_age, new_surname, new_age):
        print('Zmiany: {} nazwisko: {} na {}'
              .format(item, old_surname, new_surname))
        print('Zmiany: {} wiek: {} na {}'
              .format(item, old_age, new_age))

    @staticmethod
    def user_deleted(name):
        print('Usunąłeś {} z bazy'.format(name))
