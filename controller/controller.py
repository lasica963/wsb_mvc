from factories import factories


class Controller(object):

    def __init__(self, model, view):
        self.model = model
        self.view = view

    def get_people(self):
        items = self.model.get_people()
        self.view.show_person_list(items)

    def get_user(self, item_name):
        try:
            item = self.model.read_item(item_name)
            self.view.show_person(item_name, item)
        except factories.ItemNotExists as e:
            self.view.user_missing_error(item_name, e)

    def add_person(self, name, surname, age):
        assert age > 0, 'wiek musi być większy od 0'
        try:
            self.model.add_person(name, surname, age)
            self.view.user_storied(name)
        except factories.ItemAlreadyExists as e:
            self.view.user_already_stored_error(name, item_type, e)

    def update_user(self, name, surname, age):
        assert age > 0, 'wiek musi być większy niż 0'
        try:
            older = self.model.read_item(name)
            self.model.update_user(name, surname, age)
            self.view.user_updated(
                name, older['surname'], older['age'], surname, age)
        except factories.ItemNotExists as e:
            self.view.user_not_yet_stored_error(name, e)

    def delete_user(self, name):
        try:
            self.model.delete_user(name)
            self.view.user_deleted(name)
        except factories.ItemNotExists as e:
            self.view.user_not_yet_stored_error(name, item_type, e)
