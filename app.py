from controller import controller
from model import model
from view import view


def main():
    users = [
        {'name': 'Łukasz', 'surname': 'Kowalski', 'age': 20},
        {'name': 'Łukasz', 'surname': 'Nowak', 'age': 22},
        {'name': 'Łukasz', 'surname': 'Anonim', 'age': 24},
        {'name': 'Adam', 'surname': 'Anonim', 'age': 5},
    ]

    action = controller.Controller(model.ModelBasic(users), view.View())

    # Odkomentuj kod, który chcesz przetestować

    # Wyświetl wszystkich userów
    # action.get_people()

    # Dodaj usera Adam i pobierz z bazy
    # print('-----------')
    # print('Lista przed dodaniem usera:')
    # print('-----------')
    # action.get_people()
    # print('-----------')
    # action.add_person('adam', 'kozak', 23)
    # action.get_user('adam')
    # print('-----------')
    # print('Oto aktualna lista:')
    # print('-----------')
    # action.get_people()

    # Zaaktuaizauj usera Adam
    # action.get_user('Adam')
    # print('-----------')
    # action.update_user('Adam', 'Nowe nazwisko', 44)
    # print('-----------')
    # action.get_user('Adam')

    # Usuń usera Adam z bazy
    # action.get_people()
    # print('-----------')
    # action.delete_user('Adam')
    # print('-----------')
    # action.get_people()


if __name__ == '__main__':
    main()
