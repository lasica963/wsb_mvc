import backend


class ModelBasic(object):

    def __init__(self, application_items):
        self._item_type = 'person'
        self.create_items(application_items)

    def add_person(self, name, surname, age):
        backend.add_person(name, surname, age)

    def create_items(self, items):
        backend.create_items(items)

    def read_item(self, name):
        return backend.read_item(name)

    def get_people(self):
        return backend.get_people()

    def update_user(self, name, surname, age):
        backend.update_user(name, surname, age)

    def delete_user(self, name):
        backend.delete_user(name)
